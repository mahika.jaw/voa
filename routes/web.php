<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
// use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminSliderController;
use App\Http\Controllers\Admin\AdminAboutController;
use App\Http\Controllers\Admin\AdminCounterController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\BeavoiceController;
use App\Http\Controllers\Front\ContactController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\User\PdfController;
use App\Http\Controllers\User\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [HomeController::class, 'index'])->name('mainhome');
Route::get('/set-locale/{locale}', [LocalizationController::class, 'setLocale'])->name('setLocale');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/be-a-voice', [BeavoiceController::class, 'index'])->name('beavoice');
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::middleware('auth')->prefix('user')->group(function () {
    Route::get('/user_dashboard', [UserController::class, 'dashboard'])->name('user_dashboard');
    Route::get('/payments', [UserController::class, 'payments'])->name('payments');
    Route::get('/pdf/download/{filename}', [PdfController::class, 'download_pdf'])->name('pdf.download');
    Route::get('/profile', [UserController::class, 'profile'])->name('user_profile');
    Route::post('/profile-submit', [UserController::class, 'profile_submit'])->name('user_edit_profile_submit');
});


Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/dashboard', function () {
        return view('admin.index');
    });
});

/* Admin */
Route::middleware('admin')->prefix('admin')->group(function () {
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('admin_dashboard');
    Route::get('/edit-profile', [AdminController::class, 'edit_profile'])->name('admin_edit_profile');
    Route::post('/edit-profile-submit', [AdminController::class, 'edit_profile_submit'])->name('admin_edit_profile_submit');
    Route::get('/slider/index', [AdminSliderController::class, 'index'])->name('admin_slider_index');
    Route::get('/slider/create', [AdminSliderController::class, 'create'])->name('admin_slider_create');
    Route::post('/slider/create/submit', [AdminSliderController::class, 'create_submit'])->name('admin_slider_create_submit');
    Route::get('/slider/edit{id}', [AdminSliderController::class, 'edit'])->name('admin_slider_edit');
    Route::post('/slider/edit/submit/{id}', [AdminSliderController::class, 'edit_submit'])->name('admin_slider_edit_submit');
    Route::get('/slider/delete{id}', [AdminSliderController::class, 'delete'])->name('admin_slider_delete');


    Route::get('/about/edit', [AdminAboutController::class, 'edit'])->name('admin_about_edit');
    Route::post('/about/edit/submit', [AdminAboutController::class, 'edit_submit'])->name('admin_about_edit_submit');

    Route::get('/counter/edit', [AdminCounterController::class, 'edit'])->name('admin_counter_edit');
    Route::post('/counter/edit/submit', [AdminCounterController::class, 'edit_submit'])->name('admin_counter_edit_submit');
});

Route::prefix('admin')->group(function () {
    Route::get('/login', [AdminController::class, 'login'])->name('admin_login');
    Route::post('/login-submit', [AdminController::class, 'login_submit'])->name('admin_login_submit');
    Route::get('/logout', [AdminController::class, 'logout'])->name('admin_logout');
});


Route::post('payment', [\App\Http\Controllers\Front\PayPalController::class, 'payment'])->name('payment');
Route::get('cancel', [\App\Http\Controllers\Front\PayPalController::class, 'cancel'])->name('payment.cancel');
Route::get('payment/success', [\App\Http\Controllers\Front\PayPalController::class, 'success'])->name('payment.success');


Route::post('paypal/payment', [\App\Http\Controllers\Front\PayPalController::class, 'paypal_payment']);
