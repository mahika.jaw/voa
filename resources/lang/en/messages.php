
<?php
return [
    'welcome' => 'Welcome to our application',
    'sliderheading' => 'Stand with Amhara: Hope in the Heart of Ethiopia',
    'sliderprargraph' => 'Together for Amhara: Healing, Rebuilding, Empowering'
];
