<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="{{ url('public/dist-front/lib/easing/easing.min.js') }}"></script>
<script src="{{ url('public/dist-front/lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ url('public/dist-front/lib/waypoints/waypoints.min.js') }}"></script>
<script src="{{ url('public/dist-front/lib/counterup/counterup.min.js') }}"></script>
<script src="{{ url('public/dist-front/lib/parallax/parallax.min.js') }}"></script>
<script src="{{ url('public/dist-admin/js/iziToast.min.js') }}"></script>

<!-- Contact Javascript File -->
<script src="{{ url('public/dist-front/mail/jqBootstrapValidation.min.js') }}"></script>
<script src="{{ url('public/dist-front/mail/contact.js') }}"></script>

<!-- Template Javascript -->
<script src="{{ url('public/dist-front/js/main.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#language-select').change(function() {
            var selectedLocale = $(this).val();
            var baseUrl = $('#url-meta').attr('content');
            // Redirect to a route to set the locale
            window.location.href = baseUrl + '/set-locale/' + selectedLocale;
        });
    });
</script>
<script>
    $(document).ready(function() {
        // Show video container on page load
        $('#video-container').show();

        // Close button functionality
        $('#close-button').click(function() {
            $('#video-container').hide();
        });
    });
</script>
@if($errors->any())
@foreach ($errors->all() as $error)
<script>
    iziToast.error({
        message: '{{ $error }}',
        position: 'topRight'
    });
</script>


@endforeach
@endif

@if(Session::has('error'))
<script>
    iziToast.error({
        message: '{{ Session::get("error") }}',
        position: 'topRight'
    });
</script>

@endif
@if(Session::has('success'))
<script>
    iziToast.success({
        message: '{{ Session::get("success") }}',
        position: 'topRight'
    });
</script>
@endif