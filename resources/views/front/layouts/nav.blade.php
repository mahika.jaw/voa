        
        <div class="latest">
            <marquee width="100%" direction="right" height="25px">
                <a href="#"><p>We are soon going live</p></a>
                </marquee>
        </div>
        <!-- Nav Bar Start -->
        <style>
            .navbar {
                background-color: #7e0000 !important;
            }
        
            .navbar-brand {
                position: absolute;
                left: 50%;
                transform: translateX(-50%);
                z-index: 2; /* Ensures logo stays above background */
            }
        
            .navbar-collapse {
                z-index: 1; /* Ensures navbar items stay above background */
            }
        </style>
        
        <div class="navbar navbar-expand-lg bg-dark navbar-dark" style="color: white;">
            <button type="button" class="navbar-toggler" style="background-color: #ffcb39; border: 1px solid #040303;" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container-fluid">
                <!-- Logo -->
                <a href="{{ route('mainhome') }}" class="navbar-brand d-flex justify-content-center align-items-center">
                    <img src="{{ url('public/uploads/img/logo_without_background.png') }}">
                    {{-- <div class="brandname"><p>Voice of Amhara</p></div> --}}
                </a>
                
                <!-- Navbar Items -->
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <!-- Left Side Menu -->
                    <div class="navbar-nav mr-auto">
                        <a href="{{ route('mainhome') }}" class="nav-item nav-link active">Home</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">News</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">1888 Radio</a>
                                <a href="#" class="dropdown-item">Twitter</a>
                            </div>
                        </div>
                        <a href="{{ route('beavoice') }}" class="nav-item nav-link">Be a Voice</a>
                        
                    </div>
                    
                    <!-- Right Side Menu -->
                    <div class="navbar-nav ml-auto">
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Amhara Genocide</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Audio Episodes</a>
                                <a href="#" class="dropdown-item">Video Episodes</a>
                                <a href="#" class="dropdown-item">Podcast</a>
                            </div>
                        </div>
                        <a href="{{ route('contact') }}" class="nav-item nav-link">Contact</a>
                        @if (Auth::guard('web')->check())
                            <a href="{{ route('user_dashboard') }}" class="nav-item nav-link">Dashboard</a>
                        @else
                            <a href="{{ route('login') }}" class="nav-item nav-link">Login</a>
                            {{-- <a href="{{ route('register') }}" class="nav-item nav-link">Sign up</a> --}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <!-- Nav Bar End -->