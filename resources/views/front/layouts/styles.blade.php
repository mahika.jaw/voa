<!-- CSS Libraries -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
<link href="{{ url('public/dist-front/lib/flaticon/font/flaticon.css') }}" rel="stylesheet">
<link href="{{ url('public/dist-front/lib/animate/animate.min.css') }}" rel="stylesheet">
<link href="{{ url('public/dist-front/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ url('public/dist-front/css/iziToast.min.css') }}">
<!-- Template Stylesheet -->
<link href="{{ url('public/dist-front/css/style.css') }}" rel="stylesheet">
<meta name="url" id="url-meta" content="{{ url('/')}}">