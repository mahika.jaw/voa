@extends('front.layouts.app')

@section('main_content')
    <!-- Page Header Start -->
    <div class="page-header" style="background: linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), url({{ url('public/uploads/img/page-header.jpg') }});
    ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Speak Up</h2>
                </div>
                <div class="col-12">
                    <a href="{{ route('mainhome') }}">Home</a>
                    <a href="#">Be a Voice</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- Event Start -->
    <div class="event">
        <div class="container">
            <div class="section-header text-center">
                <h2>A bit about how you can contribute for the cause.</h2>
                <p>This section will help you get to know about your congress man which is just a click away, Human Rights Members, EU Form, Vote for the cause & some trending #hashtags which you can post on different social media. </p>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="event-item">
                        <img src="{{ url('public/uploads/images/know-your-congress-img.png') }}" alt="Image">
                        <div class="event-content">
                            {{-- <div class="event-meta">
                                <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
                                <p><i class="far fa-clock"></i>8:00 - 10:00</p>
                                <p><i class="fa fa-map-marker-alt"></i>New York</p>
                            </div> --}}
                            <div class="event-text">
                                <h3>Don't Know Your Congress Man?</h3><br>
                                <p>
                                    Not sure of your congressional district or who your member is? This service will assist you by matching your ZIP code to your congressional district, with links to your member's website and contact page.
                                </p><br>
                                <a class="btn btn-custom" target="_blank" href="https://www.house.gov/representatives/find-your-representative">Search by Zip</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="event-item">
                        {{-- <img src="img/event-2.jpg" alt="Image"> --}}
                        <div class="event-content">
                            {{-- <div class="event-meta">
                                <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
                                <p><i class="far fa-clock"></i>8:00 - 10:00</p>
                                <p><i class="fa fa-map-marker-alt"></i>New York</p>
                            </div> --}}
                            <div class="event-text">
                                <h3>Human Rights Members</h3>
                                <p>
                                    Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
                                </p>
                                <a class="btn btn-custom" href="">Join Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="event-item">
                        {{-- <img src="img/event-2.jpg" alt="Image"> --}}
                        <div class="event-content">
                            {{-- <div class="event-meta">
                                <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
                                <p><i class="far fa-clock"></i>8:00 - 10:00</p>
                                <p><i class="fa fa-map-marker-alt"></i>New York</p>
                            </div> --}}
                            <div class="event-text">
                                <h3>EU Members</h3>
                                <p>
                                    Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
                                </p>
                                <button type="button" class="btn btn-custom"  data-toggle="modal" data-src="#" data-target="#eu_form">Join Now</button>
                            </div>
                        </div>
                    </div>
                    <div class="event-item">
                        {{-- <img src="img/event-2.jpg" alt="Image"> --}}
                        <div class="event-content">
                            {{-- <div class="event-meta">
                                <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
                                <p><i class="far fa-clock"></i>8:00 - 10:00</p>
                                <p><i class="fa fa-map-marker-alt"></i>New York</p>
                            </div> --}}
                            <div class="event-text">
                                <h3>Vote For Cause</h3>
                                <p>
                                    Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
                                </p>
                                <a class="btn btn-custom" href="">Join Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single">
                <div class="single-tags">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">"Never again" is the rallying cry for all who believe that mankind must speak out against Genocide.</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Together we can prevent Genocide from happening again.</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">We have to understand what caused genocide to happen. Or it will happen again</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    {{-- <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">#Trades</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">#Economics</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">#Politics</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">#Lifestyle</a>
                        <div class="dropdown-menu">
                            <a href="contact.html" class="dropdown-item">Twitter</a>
                            <a href="1888radio.html" class="dropdown-item">Instagram</a>
                            <a href="twitter.html" class="dropdown-item">Facebook</a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Event End -->
  
    <!-- Modal -->
    <div class="modal fade" id="eu_form" tabindex="-1" role="dialog" aria-labelledby="eu_formLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="eu_formLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4">
                    </div>
                    <div class="form-group col-md-6">
                    <label for="inputPassword4">Password</label>
                    <input type="password" class="form-control" id="inputPassword4">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputCity">City</label>
                    <input type="text" class="form-control" id="inputCity">
                    </div>
                    <div class="form-group col-md-3">
                    <label for="inputState">State</label>
                    <select id="inputState" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                    </div>
                    <div class="form-group col-md-3">
                    <label for="inputZip">Zip</label>
                    <input type="text" class="form-control" id="inputZip">
                    </div>
                </div>
                <button type="submit" class="btn btn-custom">Sign in</button>
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection