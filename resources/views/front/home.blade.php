@extends('front.layouts.app')

@section('main_content')
<!-- Video player container -->
        <div id="video-container">
            <div id="close-button">X</div>
            <iframe id="youtube-video" width="170" height="90" src="https://www.youtube.com/embed/uWlJz9bz99Y?autoplay=1&mute=1" title="1888 Radio ዕለታዊ ዜና" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
            </iframe>
        </div>
<!-- Video player container end -->
<!-- Carousel Start -->
<div class="carousel">
    <div class="container-fluid">
        <div class="owl-carousel">
            @foreach($sliders as $slider)
            <div class="carousel-item">
                <div class="carousel-img">
                    <img src="{{ url('public/uploads/'.$slider->photo) }}" alt="Image">
                </div>
                <div class="carousel-text">
                    <h1>@lang('messages.sliderheading')</h1>
                    <p>
                        @lang('messages.sliderprargraph')
                    </p>
                    <div class="carousel-btn">
                        <a class="btn btn-custom" href="{{ $slider->button_link }}donate_now">{{ $slider->button_text }}</a>
                        <a class="btn btn-custom btn-play" data-toggle="modal" data-src="#" data-target="#videoModal">Watch Video</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Carousel End -->

<!-- Video Modal Start-->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Video Modal End -->


<!-- About Start -->
<div class="about">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-img" style="background-image:url('{{ url('public/uploads/'.$about->photo) }}"></div>
            </div>
            <div class="col-lg-6">
                <div class="section-header">
                    <p>{{ $about->sub_heading }}</p>
                    <h2>{{ $about->heading }}</h2>
                </div>
                <div class="about-tab">
                    <ul class="nav nav-pills nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#tab-content-1">{{ $about->tab1 }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#tab-content-2">{{ $about->tab2 }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#tab-content-3">{{ $about->tab3 }}</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab-content-1" class="container tab-pane active">
                            {{ $about->tab1_content }}
                        </div>
                        <div id="tab-content-2" class="container tab-pane fade">
                            {{ $about->tab2_content }}
                        </div>
                        <div id="tab-content-3" class="container tab-pane fade">
                            {{ $about->tab3_content }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->


<!-- Service Start -->
<div class="service">
    <div class="container">
        <div class="section-header text-center">
            <p>What We Do?</p>
            <h2>We believe that we can save more lifes with you</h2>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-diet"></i>
                    </div>
                    <div class="service-text">
                        <h3>Healthy Food</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-water"></i>
                    </div>
                    <div class="service-text">
                        <h3>Pure Water</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-healthcare"></i>
                    </div>
                    <div class="service-text">
                        <h3>Health Care</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-education"></i>
                    </div>
                    <div class="service-text">
                        <h3>Primary Education</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-home"></i>
                    </div>
                    <div class="service-text">
                        <h3>Residence Facilities</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="service-item">
                    <div class="service-icon">
                        <i class="flaticon-social-care"></i>
                    </div>
                    <div class="service-text">
                        <h3>Social Care</h3>
                        <p>Lorem ipsum dolor sit amet elit. Phase nec preti facils ornare velit non metus tortor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Service End -->


<!-- Facts Start -->
<div class="facts" style="background-image:url('{{ url('public/uploads/img/facts.jpg') }}')">
    <div class="container">
        <div class="row">
            {{-- <div class="col-lg-3 col-md-6">
                <div class="facts-item">
                    <i class="flaticon-home"></i>
                    <div class="facts-text">
                        <h3 class="facts-plus" data-toggle="counter-up">{{ $counter->counter1_number }}</h3>
                        <p>{{ $counter->counter1_name }}</p>
                    </div>
                </div>
            </div> --}}
            <div class="col-lg-6 col-md-6">
                <div class="facts-item">
                    <i class="flaticon-charity"></i>
                    <div class="facts-text">
                        <h3 class="facts-plus" data-toggle="counter-up">{{ $counter->counter2_number }}</h3>
                        <p>{{ $counter->counter2_name }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="facts-item">
                    <i class="flaticon-kindness"></i>
                    <div class="facts-text">
                        <h3 class="facts-dollar" data-toggle="counter-up">{{ $counter->counter3_number }}</h3>
                        <p>{{ $counter->counter3_name }}</p>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-3 col-md-6">
                <div class="facts-item">
                    <i class="flaticon-donation"></i>
                    <div class="facts-text">
                        <h3 class="facts-dollar" data-toggle="counter-up">{{ $counter->counter4_number }}</h3>
                        <p>{{ $counter->counter4_name }}</p>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</div>
<!-- Facts End -->


<!-- Causes Start -->
{{-- <div class="causes">
            <div class="container">
                <div class="section-header text-center">
                    <p>Popular Causes</p>
                    <h2>Let's know about charity causes around the world</h2>
                </div>
                <div class="owl-carousel causes-carousel">
                    <div class="causes-item">
                        <div class="causes-img">
                            <img src="{{ url('public/uploads/img/causes-1.jpg') }}" alt="Image">
</div>
<div class="causes-progress">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
            <span>85%</span>
        </div>
    </div>
    <div class="progress-text">
        <p><strong>Raised:</strong> $100000</p>
        <p><strong>Goal:</strong> $50000</p>
    </div>
</div>
<div class="causes-text">
    <h3>Lorem ipsum dolor sit</h3>
    <p>Lorem ipsum dolor sit amet elit. Phasell nec pretium mi. Curabit facilis ornare velit non vulputa</p>
</div>
<div class="causes-btn">
    <a class="btn btn-custom">Learn More</a>
    <a class="btn btn-custom">Donate Now</a>
</div>
</div>
<div class="causes-item">
    <div class="causes-img">
        <img src="{{ url('public/uploads/img/causes-2.jpg') }}" alt="Image">
    </div>
    <div class="causes-progress">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                <span>85%</span>
            </div>
        </div>
        <div class="progress-text">
            <p><strong>Raised:</strong> $100000</p>
            <p><strong>Goal:</strong> $50000</p>
        </div>
    </div>
    <div class="causes-text">
        <h3>Lorem ipsum dolor sit</h3>
        <p>Lorem ipsum dolor sit amet elit. Phasell nec pretium mi. Curabit facilis ornare velit non vulputa</p>
    </div>
    <div class="causes-btn">
        <a class="btn btn-custom">Learn More</a>
        <a class="btn btn-custom">Donate Now</a>
    </div>
</div>
<div class="causes-item">
    <div class="causes-img">
        <img src="{{ url('public/uploads/img/causes-3.jpg') }}" alt="Image">
    </div>
    <div class="causes-progress">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                <span>85%</span>
            </div>
        </div>
        <div class="progress-text">
            <p><strong>Raised:</strong> $100000</p>
            <p><strong>Goal:</strong> $50000</p>
        </div>
    </div>
    <div class="causes-text">
        <h3>Lorem ipsum dolor sit</h3>
        <p>Lorem ipsum dolor sit amet elit. Phasell nec pretium mi. Curabit facilis ornare velit non vulputa</p>
    </div>
    <div class="causes-btn">
        <a class="btn btn-custom">Learn More</a>
        <a class="btn btn-custom">Donate Now</a>
    </div>
</div>
<div class="causes-item">
    <div class="causes-img">
        <img src="{{ url('public/uploads/img/causes-4.jpg') }}" alt="Image">
    </div>
    <div class="causes-progress">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                <span>85%</span>
            </div>
        </div>
        <div class="progress-text">
            <p><strong>Raised:</strong> $100000</p>
            <p><strong>Goal:</strong> $50000</p>
        </div>
    </div>
    <div class="causes-text">
        <h3>Lorem ipsum dolor sit</h3>
        <p>Lorem ipsum dolor sit amet elit. Phasell nec pretium mi. Curabit facilis ornare velit non vulputa</p>
    </div>
    <div class="causes-btn">
        <a class="btn btn-custom">Learn More</a>
        <a class="btn btn-custom">Donate Now</a>
    </div>
</div>
</div>
</div>
</div> --}}
<!-- Causes End -->

<!-- Team Start -->
<div class="team">
    <div class="container">
        <div class="section-header text-center">
            <p>Meet Our Team</p>
            <h2>Awesome guys behind our charity activities</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="{{ url('public/uploads/img/team-1.jpg') }}" alt="Team Image">
                    </div>
                    <div class="team-text">
                        <h2>Donald John</h2>
                        <p>Founder & CEO</p>
                        <div class="team-social">
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="{{ url('public/uploads/img/team-2.jpg') }}" alt="Team Image">
                    </div>
                    <div class="team-text">
                        <h2>Adam Phillips</h2>
                        <p>Chef Executive</p>
                        <div class="team-social">
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="{{ url('public/uploads/img/team-3.jpg') }}" alt="Team Image">
                    </div>
                    <div class="team-text">
                        <h2>Thomas Olsen</h2>
                        <p>Chef Advisor</p>
                        <div class="team-social">
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="{{ url('public/uploads/img/team-4.jpg') }}" alt="Team Image">
                    </div>
                    <div class="team-text">
                        <h2>James Alien</h2>
                        <p>Advisor</p>
                        <div class="team-social">
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Team End -->

<!-- Donate Start -->
<!-- <div class="donate" style="background-image:url('{{ url('public/uploads/img/donate.jpg') }}');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="donate-content">
                    <div class="section-header">
                        <p>Donate Now</p>
                        <h2>Let's donate to needy people for better lives</h2>
                    </div>
                    <div class="donate-text">
                        <p>
                            Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non. Aliquam metus tortor, auctor id gravida, viverra quis sem. Curabitur non nisl nec nisi maximus. Aenean convallis porttitor. Aliquam interdum at lacus non blandit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="donate-form">
                    <form action="{{ route('payment') }}" method="post">
                        @csrf
                        <div class="control-group">
                            <input type="text" class="form-control" placeholder="Name" value="dp" />
                        </div>
                        <div class="control-group">
                            <input type="email" class="form-control" placeholder="Email" value="demo@gmail.com" />
                        </div>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-custom active">
                                <input type="radio" name="amount" checked value="10"> $10
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="amount" value="30"> $20
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="amount" value="10"> $30
                            </label>
                        </div>
                        <div>
                            <button class="btn btn-custom" type="submit">Donate Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Donate End -->

<!-- Donate Start -->
<div class="donate" id="donate_now" style="background-image:url('{{ url('public/uploads/img/donate.jpg') }}');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <div class="donate-content">
                    <div class="section-header">
                        <p>Donate Now</p>
                        <h2>Let's donate to needy people for better lives</h2>
                    </div>
                    <div class="donate-text">
                        <p>
                            Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non. Aliquam metus tortor, auctor id gravida, viverra quis sem. Curabitur non nisl nec nisi maximus. Aenean convallis porttitor. Aliquam interdum at lacus non blandit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 align-items-center" style="text-align: center;">
                <div class="donate-form">
                    <form action="{{ route('payment') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <h5 style="color: white;">One Time Donate</h5>
                        @auth
                        <p id="authStatus" hidden>true</p>
                        <input id="pay-user-id" hidden type="text" value="{{ auth()->user()->id }}">
                        <div class="control-group">
                            <input id="pay-name" name="name" type="text" class="form-control" placeholder="Name" value="{{ auth()->user()->name }}" required />
                        </div>
                        <div class="control-group">
                            <input id="pay-email" name="email" type="email" class="form-control" placeholder="Email" value="{{ auth()->user()->email }}" required />
                        </div>
                        @else
                        <p id="authStatus" hidden>false</p>
                        <div class="control-group">
                            <input id="pay-name" name="name" type="text" class="form-control" placeholder="Name" value="" required />
                        </div>
                        <div class="control-group">
                            <input id="pay-email" name="email" type="email" class="form-control" placeholder="Email" value="" required />
                        </div>
                        @endauth
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-custom active">
                                <input type="radio" name="amount" id="amount10" checked value="10"> $10
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="amount" id="amount20" value="20"> $20
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="amount" id="amount30" value="30"> $30
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="amount" id="customAmountRadio" value="custom">Amount
                            </label>
                        </div>
                        <div id="customAmountContainer" style="display: none;">
                            <input class="form-control" type="text" name="customAmount" id="customAmount" placeholder="Enter custom amount">
                        </div>
                        <script>
                            document.addEventListener("DOMContentLoaded", function() {
                                var customAmountRadio = document.getElementById("customAmountRadio");
                                var customAmountContainer = document.getElementById("customAmountContainer");

                                // Check if customAmountRadio is checked on page load
                                if (customAmountRadio.checked) {
                                    customAmountContainer.style.display = "block";
                                }

                                // Add click event listeners to all radio buttons
                                var radioButtons = document.querySelectorAll('input[name="amount"]');
                                radioButtons.forEach(function(radio) {
                                    radio.addEventListener("click", function() {
                                        // Hide customAmountContainer when any other radio button is clicked
                                        customAmountContainer.style.display = customAmountRadio.checked ? "block" : "none";
                                    });
                                });
                            });
                        </script>
                        <div class="d-flex">
                            <button class="btn btn-custom" type="button">Zelle</button>
                            <button class="btn btn-custom" type="button">Cash app</button>
                        </div>
                        <div style="margin: 6px;">
                            <button class="btn btn-custom" type="submit">Donate Now</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 align-items-center" style="text-align: center;">
                <div class="donate-form">
                    <form action="{{ route('payment') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <h5 style="color: white;">Become a Subscriber</h5>
                        @auth
                        <p id="authStatus" hidden>true</p>
                        <input id="pay-user-id" hidden type="text" value="{{ auth()->user()->id }}">
                        <div class="control-group">
                            <input id="pay-name" name="name" type="text" class="form-control" placeholder="Name" value="{{ auth()->user()->name }}" required />
                        </div>
                        <div class="control-group">
                            <input id="pay-email" name="email" type="email" class="form-control" placeholder="Email" value="{{ auth()->user()->email }}" required />
                        </div>
                        @else
                        <p id="authStatus" hidden>false</p>
                        <div class="control-group">
                            <input id="pay-name" name="name" type="text" class="form-control" placeholder="Name" value="" required />
                        </div>
                        <div class="control-group">
                            <input id="pay-email" name="email" type="email" class="form-control" placeholder="Email" value="" required />
                        </div>
                        @endauth
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-custom active">
                                <input type="radio" name="p-amount" id="amount10" checked value="10"> $10
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="p-amount" id="amount20" value="20"> $20
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="p-amount" id="amount30" value="30"> $30
                            </label>
                            <label class="btn btn-custom">
                                <input type="radio" name="p-amount" id="p-customAmountRadio" value="custom">Amount
                            </label>
                        </div>
                        <div id="p-customAmountContainer" style="display: none;">
                            <input class="form-control" type="text" name="customAmount" id="customAmount" placeholder="Enter custom amount">
                        </div>
                        <script>
                            document.addEventListener("DOMContentLoaded", function() {
                                var customAmountRadio = document.getElementById("p-customAmountRadio");
                                var customAmountContainer = document.getElementById("p-customAmountContainer");

                                // Check if customAmountRadio is checked on page load
                                if (customAmountRadio.checked) {
                                    customAmountContainer.style.display = "block";
                                }
                                // Add click event listeners to all radio buttons
                                var radioButtons = document.querySelectorAll('input[name="p-amount"]');
                                radioButtons.forEach(function(radio) {
                                    radio.addEventListener("click", function() {
                                        // Hide customAmountContainer when any other radio button is clicked
                                        customAmountContainer.style.display = customAmountRadio.checked ? "block" : "none";
                                    });
                                });
                            });
                        </script>
                        <br>
                        <script src="https://www.paypalobjects.com/donate/sdk/donate-sdk.js" charset="UTF-8"></script>
                        <div onclick="checkAuthentication()" id="paypal-donate-button-container"></div>
                        <script>
                            var config = {
                                image: {
                                    // src: 'https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif',
                                    src: 'https://pics-v2.sandbox.paypal.com/00/s/NGU3MjVjNDgtMDZiMi00N2MxLWE4YmUtMzBiMzU0NTNmM2Fk/file.PNG',
                                    title: 'PayPal - The safer, easier way to pay online!',
                                    alt: 'Donate with PayPal button'
                                },
                            };
                            var authStatusElement = document.getElementById('authStatus');
                            if (authStatusElement.innerText == 'true') {
                                config = {
                                    env: 'sandbox',
                                    hosted_button_id: 'LSR7RPJJ73DWE',
                                    // business: 'YOUR_EMAIL_OR_PAYERID',
                                    image: {
                                        src: 'https://pics-v2.sandbox.paypal.com/00/s/NGU3MjVjNDgtMDZiMi00N2MxLWE4YmUtMzBiMzU0NTNmM2Fk/file.PNG',
                                        title: 'PayPal - The safer, easier way to pay online!',
                                        alt: 'Donate with PayPal button'
                                    },
                                    onComplete: function(params) {
                                        var userIdInput = document.getElementById('pay-user-id');
                                        var nameInput = document.getElementById('pay-name');
                                        var emailInput = document.getElementById('pay-email');

                                        var userIdValue = Number(userIdInput.value);
                                        var nameValue = nameInput.value;
                                        var emailValue = emailInput.value;
                                        var csrfToken = document.querySelector('input[name="_token"]').value;

                                        fetch('http://localhost/eshop/paypal/payment', {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                    'X-CSRF-TOKEN': csrfToken
                                                },
                                                body: JSON.stringify({
                                                    ...params,
                                                    name: nameValue,
                                                    email: emailValue,
                                                    user_id: userIdValue
                                                }),
                                            })
                                            .then(response => response.json())
                                            .then(data => {
                                                console.log('Success:', data);
                                            })
                                            .catch((error) => {
                                                console.error('Error:', error);
                                            });
                                    },
                                }
                            }

                            function checkAuthentication() {
                                var authStatusElement = document.getElementById('authStatus');
                                if (authStatusElement.innerText == 'false') {
                                    window.location.href = 'http://localhost/eshop/login';
                                }
                            }
                            PayPal.Donation.Button(config).render('#paypal-donate-button-container');
                        </script>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Donate End -->


<!-- Event Start -->
{{-- <div class="event">
            <div class="container">
                <div class="section-header text-center">
                    <p>Upcoming Events</p>
                    <h2>Be ready for our upcoming charity events</h2>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="event-item">
                            <img src="{{ url('public/uploads/img/event-1.jpg') }}" alt="Image">
<div class="event-content">
    <div class="event-meta">
        <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
        <p><i class="far fa-clock"></i>8:00 - 10:00</p>
        <p><i class="fa fa-map-marker-alt"></i>New York</p>
    </div>
    <div class="event-text">
        <h3>Lorem ipsum dolor sit</h3>
        <p>
            Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
        </p>
        <a class="btn btn-custom" href="">Join Now</a>
    </div>
</div>
</div>
</div>
<div class="col-lg-6">
    <div class="event-item">
        <img src="{{ url('public/uploads/img/event-2.jpg') }}" alt="Image">
        <div class="event-content">
            <div class="event-meta">
                <p><i class="fa fa-calendar-alt"></i>01-Jan-45</p>
                <p><i class="far fa-clock"></i>8:00 - 10:00</p>
                <p><i class="fa fa-map-marker-alt"></i>New York</p>
            </div>
            <div class="event-text">
                <h3>Lorem ipsum dolor sit</h3>
                <p>
                    Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
                </p>
                <a class="btn btn-custom" href="">Join Now</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div> --}}
<!-- Event End -->



<!-- Volunteer Start -->
{{-- <div class="volunteer" style="background-image:url('{{ url('public/uploads/img/volunteer.jpg') }}">
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-5">
            <div class="volunteer-form">
                <form>
                    <div class="control-group">
                        <input type="text" class="form-control" placeholder="Name" required="required" />
                    </div>
                    <div class="control-group">
                        <input type="email" class="form-control" placeholder="Email" required="required" />
                    </div>
                    <div class="control-group">
                        <textarea class="form-control" placeholder="Why you want to become a volunteer?" required="required"></textarea>
                    </div>
                    <div>
                        <button class="btn btn-custom" type="submit">Become a volunteer</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="volunteer-content">
                <div class="section-header">
                    <p>Become A Volunteer</p>
                    <h2>Let’s make a difference in the lives of others</h2>
                </div>
                <div class="volunteer-text">
                    <p>
                        Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non. Aliquam metus tortor, auctor id gravida, viverra quis sem. Curabitur non nisl nec nisi maximus. Aenean convallis porttitor. Aliquam interdum at lacus non blandit.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</div> --}}
<!-- Volunteer End -->


<!-- Testimonial Start -->
{{-- <div class="testimonial">
            <div class="container">
                <div class="section-header text-center">
                    <p>Testimonial</p>
                    <h2>What people are talking about our charity activities</h2>
                </div>
                <div class="owl-carousel testimonials-carousel">
                    <div class="testimonial-item">
                        <div class="testimonial-profile">
                            <img src="{{ url('public/uploads/img/testimonial-1.jpg') }}" alt="Image">
<div class="testimonial-name">
    <h3>Person Name</h3>
    <p>Profession</p>
</div>
</div>
<div class="testimonial-text">
    <p>
        Lorem ipsum dolor sit amet elit. Phasel preti mi facilis ornare velit non vulputa. Aliqu metus tortor, auctor id gravid vivera quis
    </p>
</div>
</div>
<div class="testimonial-item">
    <div class="testimonial-profile">
        <img src="{{ url('public/uploads/img/testimonial-2.jpg') }}" alt="Image">
        <div class="testimonial-name">
            <h3>Person Name</h3>
            <p>Profession</p>
        </div>
    </div>
    <div class="testimonial-text">
        <p>
            Lorem ipsum dolor sit amet elit. Phasel preti mi facilis ornare velit non vulputa. Aliqu metus tortor, auctor id gravid vivera quis
        </p>
    </div>
</div>
<div class="testimonial-item">
    <div class="testimonial-profile">
        <img src="{{ url('public/uploads/img/testimonial-3.jpg') }}" alt="Image">
        <div class="testimonial-name">
            <h3>Person Name</h3>
            <p>Profession</p>
        </div>
    </div>
    <div class="testimonial-text">
        <p>
            Lorem ipsum dolor sit amet elit. Phasel preti mi facilis ornare velit non vulputa. Aliqu metus tortor, auctor id gravid vivera quis
        </p>
    </div>
</div>
<div class="testimonial-item">
    <div class="testimonial-profile">
        <img src="{{ url('public/uploads/img/testimonial-4.jpg') }}" alt="Image">
        <div class="testimonial-name">
            <h3>Person Name</h3>
            <p>Profession</p>
        </div>
    </div>
    <div class="testimonial-text">
        <p>
            Lorem ipsum dolor sit amet elit. Phasel preti mi facilis ornare velit non vulputa. Aliqu metus tortor, auctor id gravid vivera quis
        </p>
    </div>
</div>
</div>
</div>
</div> --}}
<!-- Testimonial End -->


<!-- Contact Start -->
<div class="contact">
    <div class="container">
        <div class="section-header text-center">
            <p>Get In Touch</p>
            <h2>Contact for any query</h2>
        </div>
        <div class="contact-img">
            <img src="{{ url('public/uploads/img/contact.jpg') }}" alt="Image">
        </div>
        <div class="contact-form">
            <div id="success"></div>
            <form name="sentMessage" id="contactForm" novalidate="novalidate">
                <div class="control-group">
                    <input type="text" class="form-control" id="name" placeholder="Your Name" required="required" data-validation-required-message="Please enter your name" />
                    <p class="help-block text-danger"></p>
                </div>
                <div class="control-group">
                    <input type="email" class="form-control" id="email" placeholder="Your Email" required="required" data-validation-required-message="Please enter your email" />
                    <p class="help-block text-danger"></p>
                </div>
                <div class="control-group">
                    <input type="text" class="form-control" id="subject" placeholder="Subject" required="required" data-validation-required-message="Please enter a subject" />
                    <p class="help-block text-danger"></p>
                </div>
                <div class="control-group">
                    <textarea class="form-control" id="message" placeholder="Message" required="required" data-validation-required-message="Please enter your message"></textarea>
                    <p class="help-block text-danger"></p>
                </div>
                <div>
                    <button class="btn btn-custom" type="submit" id="sendMessageButton">Send Message</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Contact End -->


<!-- Blog Start -->
{{-- <div class="blog">
            <div class="container">
                <div class="section-header text-center">
                    <p>Our Blog</p>
                    <h2>Latest news & articles directly from our blog</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="{{ url('public/uploads/img/blog-1.jpg') }}" alt="Image">
</div>
<div class="blog-text">
    <h3><a href="#">Lorem ipsum dolor sit</a></h3>
    <p>
        Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
    </p>
</div>
<div class="blog-meta">
    <p><i class="fa fa-user"></i><a href="">Admin</a></p>
    <p><i class="fa fa-comments"></i><a href="">15 Comments</a></p>
</div>
</div>
</div>
<div class="col-lg-4">
    <div class="blog-item">
        <div class="blog-img">
            <img src="{{ url('public/uploads/img/blog-2.jpg') }}" alt="Image">
        </div>
        <div class="blog-text">
            <h3><a href="#">Lorem ipsum dolor sit</a></h3>
            <p>
                Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
            </p>
        </div>
        <div class="blog-meta">
            <p><i class="fa fa-user"></i><a href="">Admin</a></p>
            <p><i class="fa fa-comments"></i><a href="">15 Comments</a></p>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="blog-item">
        <div class="blog-img">
            <img src="{{ url('public/uploads/img/blog-3.jpg') }}" alt="Image">
        </div>
        <div class="blog-text">
            <h3><a href="#">Lorem ipsum dolor sit</a></h3>
            <p>
                Lorem ipsum dolor sit amet elit. Neca pretim miura bitur facili ornare velit non vulpte liqum metus tortor
            </p>
        </div>
        <div class="blog-meta">
            <p><i class="fa fa-user"></i><a href="">Admin</a></p>
            <p><i class="fa fa-comments"></i><a href="">15 Comments</a></p>
        </div>
    </div>
</div>
</div>
</div>
</div> --}}
<!-- Blog End -->

@endsection