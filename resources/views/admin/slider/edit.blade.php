@extends('admin.layouts.app');

@section('main_content')
<div class="main-content">
    <section class="section">
        <div class="section-header d-flex justify-content-between">
            <h1>Edit Sliders</h1>
            <div>
                <a href="{{ route('admin_slider_index') }}" class="btn btn-primary"><i class="fas fa-plus"></i> View All</a>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <form action="{{ route('admin_slider_edit_submit', $slider->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label>Heading *</label>
                                        <input type="text" class="form-control" name="heading" value="{{ $slider->heading }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Text *</label>
                                        <textarea name="text" class="form-control h_100" cols="30" rows="10">{{ $slider->text }}</textarea>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Button text</label>
                                        <input type="text" class="form-control" name="button_text" value="{{ $slider->button_text }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Button Link</label>
                                        <input type="text" class="form-control" name="button_link" value="{{ $slider->button_link }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Existing Photo</label>
                                        <div>
                                            <img src="{{ url('public/uploads/'.$slider->photo) }}" alt="" class="w_200">
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Change Photo</label>
                                        <div>
                                            <input type="file" name="photo">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection