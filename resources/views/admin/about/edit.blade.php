@extends('admin.layouts.app');

@section('main_content')
<div class="main-content">
    <section class="section">
        <div class="section-header d-flex justify-content-between">
            <h1>Edit About</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <form action="{{ route('admin_about_edit_submit', $about->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label>Sub Heading</label>
                                                <input type="text" class="form-control" name="sub_heading" value="{{ $about->sub_heading }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label>Heading</label>
                                                <input type="text" class="form-control" name="heading" value="{{ $about->heading }}">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group mb-3">
                                                <label>Tab 1</label>
                                                <input type="text" class="form-control" name="tab1" value="{{ $about->tab1 }}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3">
                                                <label>Tab 2</label>
                                                <input type="text" class="form-control" name="tab2" value="{{ $about->tab2 }}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                           <div class="form-group mb-3">
                                                <label>Tab 3</label>
                                                <input type="text" class="form-control" name="tab3" value="{{ $about->tab3 }}">
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label>Tab 1 content</label>
                                        <textarea name="tab1_content" class="form-control h_100" cols="30" rows="10">{{ $about->tab1_content }}</textarea>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Tab 2 content</label>
                                        <textarea name="tab2_content" class="form-control h_100" cols="30" rows="10">{{ $about->tab2_content }}</textarea>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Tab 3 content</label>
                                        <textarea name="tab3_content" class="form-control h_100" cols="30" rows="10">{{ $about->tab3_content }}</textarea>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Existing Photo</label>
                                        <div>
                                            <img src="{{ url('public/uploads/'.$about->photo) }}" alt="" class="w_200">
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Change Photo</label>
                                        <div>
                                            <input type="file" name="photo">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection