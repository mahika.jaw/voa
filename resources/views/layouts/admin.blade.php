<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <link href="{{ asset('admin/css/material-dashboard.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">


</head>
<body class="g-sidenav-show  bg-gray-200">
    @include('layouts.include.sidebar')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    @include('layouts.include.admin_nav')

    <div class="container-fluid py-4">
        @yield('content')
    @include('layouts.include.adminfooter')
    </div>
</main>
        <!-- Scripts -->
        <script src="{{ asset('admin/js/core/popper.min.js') }}" defer></script>
        <script src="{{ asset('admin/js/core/bootstrap.min.js') }}" defer></script>
        <script src="{{ asset('admin/js/core/bootstrap.bundle.min.js') }}" defer></script>
        <script src="{{ asset('admin/js/perfect-scrollbar.min.js') }}" defer></script>
        <script src="../assets/js/plugins/smooth-scrollbar.min.js"></script>
        <script src="../assets/js/plugins/chartjs.min.js"></script>
        @yield('scripts')

</body>
</html>
