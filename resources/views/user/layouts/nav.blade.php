<div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <form class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                </ul>
            </form>
            <ul class="navbar-nav navbar-right justify-content-end rightsidetop">
                <li class="nav-link">
                    <a href="{{ route('mainhome') }}" class="btn btn-warning">Go to website</a>
                </li>
                <li class="text-white pt_5">
                    Logged in as: {{ Auth::guard('web')->user()->name }}
                </li>
                <li class="text-white pt_5">
                    <a class="" style="margin: 0px 35px; color: white;" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                </li>
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        @if (Auth::guard('web')->user()->photo != null)
                        <img src="{{ url('public/uploads/'.Auth::guard('web')->user()->photo) }}" alt="" class="rounded-circle-custom">                                            
                        @else
                        <img src="{{ url('public/uploads/default.png') }}" alt="" class="rounded-circle-custom">
                        @endif
                    
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="{{ route('user_profile') }}"><i class="far fa-user"></i> Edit Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                        </li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
