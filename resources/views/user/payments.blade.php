@extends('user.layouts.app');

@section('main_content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Payments</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @if($payments->isEmpty())
                            <p>No payments made.</p>
                            @else
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Amount</th>
                                        <th>Export</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->name }}</td>
                                        <td>{{ $payment->email }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td>
                                            <a href="{{ route('pdf.download', ['filename' => pathinfo($payment->pdf_url, PATHINFO_BASENAME)]) }}" target="_blank">
                                                <button class="btn btn-primary">Download</button>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection