<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="{{ public_path('invoice.css') }}" type="text/css">
    <style>
        body {
            font-family: system-ui, system-ui, sans-serif;
        }

        table {
            border-spacing: 0;
        }

        table td,
        table th,
        p {
            font-size: 13px !important;
        }

        img {
            border: 3px solid #F1F5F9;
            padding: 6px;
            background-color: #F1F5F9;
        }

        .table-no-border {
            width: 100%;
        }

        .table-no-border .width-50 {
            width: 50%;
        }

        .table-no-border .width-70 {
            width: 70%;
            text-align: left;
        }

        .table-no-border .width-30 {
            width: 30%;
        }

        .margin-top {
            margin-top: 40px;
        }

        .product-table {
            margin-top: 20px;
            width: 100%;
            border-width: 0px;
        }

        .product-table thead th {
            background-color: #fda027;
            color: white;
            padding: 5px;
            text-align: left;
            padding: 5px 15px;
        }

        .width-20 {
            width: 20%;
        }

        .width-50 {
            width: 50%;
        }

        .width-25 {
            width: 25%;
        }

        .width-70 {
            width: 70%;
            text-align: right;
        }

        .product-table tbody td {
            background-color: #F1F5F9;
            color: black;
            padding: 5px 15px;
        }

        .product-table td:last-child,
        .product-table th:last-child {
            text-align: right;
        }

        .product-table tfoot td {
            color: black;
            padding: 5px 15px;
        }

        .footer-div {
            background-color: #F1F5F9;
            margin-top: 100px;
            padding: 3px 10px;
        }

        .footer {
            text-align: center;
            font-size: 12px;
            margin-top: 20px;
        }

        .addr-right {
            border-right: 5px solid #fda027;
        }

        .summary-left {
            border-left: 5px solid #fda027;
        }

        .summary-child {
            padding-left: 10px;
        }

        product-table
    </style>
</head>

<body>

    <table class="table-no-border">
        <tr>
            <td class="width-70">
                <img src="{{ public_path('logo.jpg') }}" alt="" width="200" />
            </td>
            <td class="width-30">
                <div class="addr-right">
                    4223 Cody Ridge Road<br>
                    Guymon, OK 73942<br>
                    Phone: 580-516-5369<br>
                    Website: www.treble.com
                </div>
            </td>
        </tr>
    </table>

    <div class="margin-top">
        <h2>Dear Customer,</h2>
        <p>Thank you for upgrading to Treble Premium. Your subscription information has been added below.</p>
        <p>If you did not initiate this subscription, please reach out to us at support@treble.com</p>
        <table class="table-no-border">
            <tr>
                <td class="width-50">
                    <div style="color:#fda027;width: 120px;font-size: 20px;margin-bottom: 2px;"><strong>SUMMARY</strong></div>
                    <div class="summary-left">
                        <div class="summary-child"><strong> DATE : </strong>{{date('y-m-d')}}</div>
                        <div class="summary-child"><strong>PAYMENTMODE : </strong>PAYPAL</div>
                        <div class="summary-child"><strong>CREDITS :</strong> 0</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div>
        <table class="product-table">
            <thead>
                <tr>
                    <th>
                        <strong>SUBSCRIPTION TYPE</strong>
                    </th>
                    <th>
                        <strong>PRICE/MONTH</strong>
                    </th>
                    <th>
                        <strong>PAID</strong>
                    </th>
                    <th>
                        <strong>VALIDITY</strong>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $value)
                <tr>
                    <td>
                        {{ $value['type'] }}
                    </td>
                    <td>
                        {{ $value['month'] }}
                    </td>
                    <td>
                        {{ $value['paid'] }}
                    </td>
                    <td>
                        {{ $value['validity'] }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <p>Your subscription will be auto-renewed before the expiry date. However, if you wish to discontinue, please remove your billing information by logging in to your account.</p>
        <p>Visit our FAQs page to know more about different subscription levels.</p>
    </div>

    <div class="footer">
        <p>Privacy Policy | Terms & Conditions | Contact</p>
        <p>&copy; Treble-All Rights Reserved</p>
    </div>

</body>

</html>