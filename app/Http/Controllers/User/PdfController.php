<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function download_pdf($filename)
    {
        $pdfPath = storage_path('app/pdfs/' . $filename);

        if (file_exists($pdfPath)) {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            return response()->file($pdfPath, $headers);
        } else {
            abort(404, 'PDF not found');
        }
    }
}
