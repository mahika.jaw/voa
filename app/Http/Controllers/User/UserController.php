<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\OrderPayment;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function dashboard()
    {
        return view('home');
    }

    public function profile()
    {
        return view('user.profile');
    }

    public function payments()
    {
        $user_id = auth()->user()->id;
        $payments = OrderPayment::where('user_id', $user_id)->get();
        return view('user.payments', compact('payments'));
    }

    public function profile_submit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
        ]);

        if ($request->photo != null) {
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ]);
            // dd($request->photo->extension());
            if (Auth::guard('web')->user()->photo != null) {
                unlink(public_path('uploads/' . Auth::guard('web')->user()->photo));
            }

            $final_name = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('uploads'), $final_name);
        } else {
            $final_name = Auth::guard('web')->user()->photo;
        }
        $user_data = User::find(Auth::guard('web')->user()->id);
        $user_data->name = $request->name;
        $user_data->email = $request->email;
        $user_data->photo = $final_name;
        $user_data->update();


        return redirect()->back()->with('success', 'Profile Updated Successfully!');
    }
}
