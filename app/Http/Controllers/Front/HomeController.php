<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Slider;
use App\Models\About;
use App\Models\Counter;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slider::get();
        $about = About::where('id', 1)->first();
        $counter = Counter::where('id', 1)->first();
        if (Auth::user()) {
            $lan = Auth::user()->locale;
        } else {
            $setting = Setting::first();
            $lan = $setting->locale;
        }
        app()->setLocale($lan);
        return view('front.home', compact('sliders', 'about', 'counter', 'lan'));
    }
}
