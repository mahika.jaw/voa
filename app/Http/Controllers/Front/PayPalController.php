<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use App\Models\OrderPayment;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Storage;
use PHPMailer\PHPMailer\PHPMailer;
use Srmklive\PayPal\Facades\PayPal;

class PayPalController extends Controller
{

    protected $provider;
    protected $config;
    public function __construct()
    {
        $this->provider = new PayPalClient;
        $this->provider->setApiCredentials(config('paypal'));
        $this->config = [
            'mode'    => 'sandbox',
            'sandbox' => [
                'client_id'         => env('PAYPAL_CLIENT_ID', ''),
                'client_secret'     => env('PAYPAL_SECRET', ''),
                'app_id'            => 'APP-80W284485P519543T',
            ],
            'payment_action' => env('payment_action'),
            'currency'       => env('CURRENCY'),
            'notify_url'     => 'https://your-site.com/paypal/notify',
            'locale'         => 'en_US',
            'validate_ssl'   => true,
        ];
    }


    public function payment(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $selectedAmount = $request->input('amount');
        if ($selectedAmount == 'custom') {
            $selectedAmount = $request->input('customAmount');
        }

        $requestedData = [
            'name' => $name,
            'email' => $email
        ];

        $provider = new PayPalClient;
        $provider->setApiCredentials($this->config);
        $paypalToken = $provider->getAccessToken();
        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('payment.success', $requestedData),
                "cancel_url" => route('payment.cancel'),
            ],
            "purchase_units" => [
                0 => [
                    'name' => $name,
                    'email' => $email,
                    "amount" => [
                        "currency_code" => "USD",
                        "value" => $selectedAmount
                    ],
                ]
            ]
        ]);
        if (isset($response['id']) && $response['id'] != null) {
            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }
            die('error Something went wrong.');
        } else {
            die('error' . $response['message'] . ' Something went wrong.');
        }

        /*$provider = new PayPalClient;

        // Through facade. No need to import namespaces
        $provider = \PayPal::setProvider();

        $provider->setApiCredentials($this->config);
        $provider->getAccessToken();
        //$provider->setCurrency(env('CURRENCY'));
       // dd($provider);

        //subscription
        $response = $provider->addProduct('Demo Product', 'Demo Product', 'SERVICE', 'SOFTWARE')
            ->addPlanTrialPricing('DAY', 7)
            ->addMonthlyPlan('Demo Plan', 'Demo Plan', 100)
            ->setReturnAndCancelUrl(route('payment.success'), route('payment.cancel'))
            ->setupSubscription('John Doe', 'john@example.com', '2025-12-10');

         dd($response);*/
    }


    public function cancel(Request $request)
    {
        dd('Your payment is canceled. You can create cancel page here.');
    }

    public function success(Request $request)
    {
        // dd($request->all());
        $provider = new PayPalClient;
        $provider->setApiCredentials($this->config);
        $provider->getAccessToken();
        $response = $provider->capturePaymentOrder($request->token);
        if (isset($response['status']) && $response['status'] == 'COMPLETED') {
            $amount = $response['purchase_units'][0]['payments']['captures'][0]['amount']['value'];
            $data = new OrderPayment;
            $data->status = $response['status'];
            $data->txn_id = $response['id'];
            $data->amount = $amount;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->payment_object = json_encode($response);
            $data->save();
            $this->sendEmail($request->email, $amount);
            //  die('success Transaction complete.');
            return redirect('/');
        } else {
            die('error' . $response['message'] . ' Something went wrong.');
        }


        // $provider = new PayPalClient;

        // // Through facade. No need to import namespaces
        // $provider = PayPal::setProvider();

        // $provider->setApiCredentials($this->config);
        // $provider->getAccessToken();

        // $response = $provider->getExpressCheckoutDetails($request->token);
        // dd($provider);
        // if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
        //     dd('Your payment was successfully. You can create success page here.');
        // }

        // dd('Something is wrong.');
    }

    public function paypal_payment(Request $request)
    {
        $payment = new OrderPayment;
        $payment->amount = floatval($request->amt);
        $payment->payment_object = json_encode($request->all());
        $payment->txn_id = $request->tx;
        $payment->status = $request->st;
        $payment->email = $request->email;
        $payment->name = $request->name;
        $payment->user_id = $request->user_id;

        $pdf_url =  $this->sendEmail($request->email, $request->amt);
        $payment->pdf_url = $pdf_url;
        $payment->save();
        return $payment;
    }

    private function sendEmail($to, $paidAmount)
    {
        $afterMonth = Carbon::now()->addMonth()->format('Y-m-d');
        $amount = "$" . $paidAmount;
        $data = ["data" => [['type' => "premium", 'month' => $amount, 'paid' => $amount, 'validity' => $afterMonth]]];
        $pdf = PDF::loadView('invoice', $data);

        $pdfPath = 'pdfs/' . uniqid() . '_example.pdf';

        Storage::put($pdfPath, $pdf->output());

        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST', 'sandbox.smtp.mailtrap.io');
            $mail->SMTPAuth   = true;
            $mail->Username   = env('MAIL_USERNAME', '216506c211157e');
            $mail->Password   = env('MAIL_PASSWORD', 'd1c9d8ebd1d132');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION', 'tls');
            $mail->Port       = env('MAIL_PORT', 587);

            $mail->setFrom(env('MAIL_FROM_ADDRESS', "hello@example.com"), env('MAIL_FROM_NAME', "Eshop"));
            $mail->addAddress($to);

            $mail->addAttachment(storage_path('app/' . $pdfPath), 'invoice.pdf');

            $mail->isHTML(true);
            $mail->Subject = "Invoice";
            $mail->Body    = "Please find the attached invoice.";
            $mail->send();

            return $pdfPath;
        } catch (Exception $e) {
            // Log the error message for debugging
            // \Log::error("Email sending failed: " . $e->getMessage());

            return false;
        }
    }
}
