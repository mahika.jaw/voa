<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;

class AdminAboutController extends Controller
{
    public function edit(){

        $about = About::where('id', 1)->first();
        return view('admin.about.edit', compact('about'));
    }

    public function edit_submit(Request $request){

        $request->validate([
            'heading' => 'required',
            'sub_heading' => 'required',
            'tab1' => 'required',
            'tab2' => 'required',
            'tab3' => 'required',
            'tab1_content' => 'required',
            'tab2_content' => 'required',
            'tab3_content' => 'required',
        ]);

        $about = About::where('id', 1)->first();

        if($request->photo != null){
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png',
            ]);

            unlink(public_path('uploads/'.$about->photo));

            $final_name = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('uploads'), $final_name);
            $about->photo = $final_name;
        }

        $about->heading = $request->heading;
        $about->sub_heading = $request->sub_heading;
        $about->tab1 = $request->tab1;
        $about->tab2 = $request->tab2;
        $about->tab3 = $request->tab3;
        $about->tab1_content = $request->tab1_content;
        $about->tab2_content = $request->tab2_content;
        $about->tab3_content = $request->tab3_content;
        $about->update();

        return redirect()->back()->with('success', 'About Section updated Successfully');
        
    }
}
