<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use app\Models\Admin;
use App\Mail\Websitemail;
use NunoMaduro\Collision\Coverage;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin.index');
    }
    public function edit_profile(){
        return view('admin.edit_profile');
    }

    public function edit_profile_submit(Request $request){
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
        ]);
        
        if($request->photo != null){
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ]);
            // dd($request->photo->extension());
            if(Auth::guard('admin')->user()->photo != null){
                unlink(public_path('uploads/'.Auth::guard('admin')->user()->photo));    
            }

            $final_name = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('uploads'), $final_name);

        }else{
            $final_name = Auth::guard('admin')->user()->photo;
        }
        $admin_data = Admin::find(Auth::guard('admin')->user()->id);
        $admin_data->name = $request->name;
        $admin_data->email = $request->email;
        $admin_data->photo = $final_name;
        $admin_data->update();


        return redirect()->back()->with('success', 'Profile Updated Successfully!');
    } 

    public function login(){
        return view('admin.login');
    }
    public function login_submit(Request $request){
        // dd($request->all());
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $check = $request->all();
        $data = [
            'email' => $check['email'],
            'password' => $check['password'],
        ];
        if(Auth::guard('admin')->attempt($data)){
            return redirect()->route('admin_dashboard')->with('success', 'Login Successfull!');
        }else{
            return redirect()->route('admin_login')->with('error', 'Invalid Credentials');
        }
    }

    public function logout(){
        // return view('admin.login');
        Auth::guard('admin')->logout();
        return redirect()->route('admin_login')->with('success', 'Logout Successful');
    }
}

