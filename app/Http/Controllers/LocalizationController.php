<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocalizationController extends Controller
{
    public function setLocale($locale)
    {
        $userLocale = $locale; // Default to the selected locale

        if (Auth::check()) { // Check if a user is authenticated
            $user = User::find(Auth::user()->id);
            $user->locale = $locale;
            $user->save();
            $userLocale = $user->locale; // Update user's locale
        } else {
            $setting = Setting::first(); // Retrieve the first setting record
            if ($setting) {
                $setting->locale = $locale;
                $setting->save();
                $userLocale = $setting->locale; // Update setting's locale
            }
        }

        app()->setLocale($userLocale); // Set the application's locale
        // dd($userLocale);
        return redirect()->route('mainhome'); // Redirect back to the previous page
    }
}
